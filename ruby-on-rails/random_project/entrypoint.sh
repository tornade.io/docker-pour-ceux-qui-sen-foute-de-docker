#!/bin/bash
set -e

until nc -z -v -w30 ${PG_HOST} ${PG_PORT}
do
  echo "Waiting for database connection..."
  sleep 2
done

bundle exec rake assets:precompile

/app/bin/rails db:prepare

exec "$@"
