#!/bin/bash

set -e # On dit à bash : si y'a la moindre erreur, tu arrete l'execution du script.

echo "Tu passeras par la dans tous les cas !"

exec "$@" # Execute ce qui est passé en paramètres
